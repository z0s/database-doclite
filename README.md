# z0s/database-doclite

### Installation
You can include it in your project using: `composer require z0s/database-doclite`

### Requirements
1. PHP8.0 or higher


### Collection example
```php
<?php

namespace z0s\Models;

use z0s\DatabaseDoclite\Collection;

class Users extends Collection
{
    /** @var string Name of collection in database */
    public string $collectionName = 'users';

    /** @var string Name of database that the collection is stored in */
    public string $databaseName = 'app';

    /** @var string Primary index key */
    public string $indexField = 'email';

    /** @var string[] $hiddenFields Fields to hide from output (ie. Password hash, email etc.) */
    public array $hiddenFields = [];

    /** @var string[] $required Fields required to insert data to model (ie. email, password hash, etc.) */
    public array $required = ['email', 'password', 'firstName', 'lastName'];
}
```

This collection can then be loaded like so..

```php

$connection = new \z0s\Database\Connection([
        [
            'host' => 'mongodb://127.0.0.1',
            'port' => 27017
        ]
    ],
    [
        'options' => [
            'connectTimeoutMS' => 30000,
            'socketTimeoutMS' => 30000,
            'serverSelectionTimeoutMS' => 30000
        ],
        'typeMap' => [
            'root' => 'object',
            'document' => 'object',
            'array' => 'object',
        ],
        'db' => 'z0s'
    ]
);

$users = new \z0s\Models\Users($connection);
```

or let your container manage loading things..

