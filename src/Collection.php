<?php

namespace z0s\DatabaseDoclite;

use Gebler\Doclite\Collection as DocliteCollection;
use Gebler\Doclite\Document;
use Gebler\Doclite\FileDatabase;
use Illuminate\Support\Collection as IlluminateCollection;

class Collection
{
    public string $collectionName = '';
    public array $hiddenFields = [];
    public array $required = [];
    public DocliteCollection $collection;
    public FileDatabase $client;

    public function __construct(
        protected Connection $connection,
    ) {
        $this->client = $this->connection->getConnection();
        $this->collection = $this->client->collection($this->collectionName);
    }


    public function find(array $filter = [], bool $showHidden = false): IlluminateCollection {
        /** @var Document $result */
        $result = $this->collection->findAllBy($filter);

        if ($showHidden) {
            return collect($result->toArray());
        }

        return (collect($result->toArray()))->forget($this->hiddenFields);
    }

    public function findOne(array $filter = [], bool $showHidden = false): IlluminateCollection {
        /** @var Document $result */
        $result = $this->collection->findOneBy($filter);

        if ($showHidden) {
            return collect($result->toArray());
        }

        return (collect($result->toArray()))->forget($this->hiddenFields);
    }

    public function findById(string $id, bool $showHidden = false): IlluminateCollection {
        $result = $this->collection->get($id);

        if ($showHidden) {
            return collect($result);
        }

        return (collect($result))->forget($this->hiddenFields);
    }

    public function count(array $filter = []): int {
        return $this->collection->findAllBy($filter)->count();
    }

    public function delete(array $filter = []) {
        /** @var Document $result */
        $result = $this->collection->findOneBy($filter);
        return $result->delete();
    }

    public function save(array $data = []) {
        $this->validate($data);

        $document = new Document($data, $this->collection);
        return $this->collection->save($document);
    }

    public function validate(array $data = []) {
        foreach ($this->required as $field) {
            if (!isset($data[$field])) {
                throw new \Exception('Missing required field: ' . $field);
            }
        }
    }
    public function truncate(): void
    {
        try {
            $this->collection->deleteAll();
        } catch (\Exception $e) {
            throw new \Exception('Error truncating collection: ' . $e->getMessage());
        }
    }
}
