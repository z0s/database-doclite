<?php

namespace z0s\DatabaseDoclite;

use Gebler\Doclite\FileDatabase;
use Psr\Log\LoggerInterface;

class Connection
{
    public function __construct(
        protected string $databasePath = '',
        protected FileDatabase $database = null,
        protected LoggerInterface $logger = null,
        protected bool $readOnly = false,
        protected bool $fullTextSearch = false,
    )
    {
        if ($this->database === null) {
            $this->database = new FileDatabase(
                path:       $this->databasePath,
                readOnly:   $this->readOnly,
                ftsEnabled: $this->fullTextSearch,
                logger:     $this->logger
            );
        }
    }

    public function getConnection(): FileDatabase
    {
        return $this->database;
    }
}
